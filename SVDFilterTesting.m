SNR = [];
CNR = [];

for cutoff = 3:100
   %[SVDData,V,D] = SVDFilter(IQData(441:856,95:161,:),1,cutoff);
   [SVDData,V,D] = SVDFilter(adaptiveDemod(117:532,95:161,:),1,cutoff);
   power = powerEstimator(SVDData,9);
   power(250:262,51) = 0;
   power(267:276,14) = 0;
   %SNR = [SNR calculateSNR(power, insideVesselMask(441:856,95:161,:),backgroundMask(441:856,95:161,:))];
   %CNR = [CNR calculateCNR(power, insideVesselMask(441:856,95:161,:),backgroundMask(441:856,95:161,:))];
   SNR = [SNR calculateSNR(power, insideVesselMask(441:856,95:161,:),backgroundMask(441:856,95:161,:))];
   CNR = [CNR calculateCNR(power, insideVesselMask(441:856,95:161,:),backgroundMask(441:856,95:161,:))];
end
SNR = SNR';
CNR = CNR';
