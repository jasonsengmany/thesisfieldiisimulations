f0 = 6.25e6; %6.25 MHz
fs = 25e6;   %25 MHz
c  = 1540;   % 1540 m/s
lambda = c/f0; % wavelength
element_height = 0.003/1000; % (m)
kerf = 0.03/1000; %Spacing between elements 
element_width = 0.27/1000;
focus = [0 0 100000]./1000; %mm
wvlToMm = 1.54/(f0*10^(-6)); % Conversion factor from wvlToMm f0 must be in MHz

set_field('fs',fs);

aperture = xdc_linear_array (128,element_width, element_height, kerf, 1, 1, focus);
receiveAperture = xdc_linear_array (128,element_width, element_height, kerf, 1, 1, focus);
impulse_response = sin(2*pi*f0*(0:1/fs:2/f0));

impulse_response=impulse_response.*hanning(max(size(impulse_response)))';
xdc_impulse (aperture, impulse_response);

excitation=sin(2*pi*f0*(0:1/fs:2/f0));
xdc_excitation (aperture, excitation);

%xdc_apodization(aperture, 0, hanning(128)');

xdc_impulse(receiveAperture, impulse_response);

points = [-10 0 20; 10 0 20; 0.1 0 20; -5 0 10; 5 0 10]./1000;
amp = [1; 1; 1; 1; 1];

ensembleLength = 1;
acquisitionLength = 9;
startAngle = -16*(pi/180)/2;
dtheta = 16*(pi/180)/(acquisitionLength-1);
delays = zeros(9,129);
%startSample = zeros(ensembleLength*acquisitionLength,1);
%endSample = zeros(ensembleLength*acquisitionLength,1);
%RcvData = zeros(1,128);
for i = 1:ensembleLength
    steerAngle = startAngle;
    for j = 1:acquisitionLength
        xdc_focus(aperture, 0, [100*sin(steerAngle) 0 100*cos(steerAngle)]); 
        [v,t1] = calc_scat_multi(aperture, aperture, points, amp);
        t0 = round(t1*fs);
        pulseLength = length(excitation) + 2*length(impulse_response) - 2;
        t0 = round(t0-pulseLength/2);
        rfdata = [zeros(t0,size(v,2));v];
        delays(j,:) = xdc_get(aperture,'focus');
        %RcvData{i,j} = v;
       % startSample((i-1)*acquisitionLength + j) = size(RcvData,1) + 1;
        RcvData{i,j} = rfdata;
       % endSample((i-1)*acquisitionLength + j) = size(RcvData,1);
        times{i, j} = t1;
        steerAngle = steerAngle + dtheta;
    end
end

