f0 = 6.25e6; %6.25 MHz
fs = 100e6;   %25 MHz
c  = 1540;   % 1540 m/s
lambda = c/f0; % wavelength
element_height = 0.003/1000; % (m)
kerf = 0.03/1000; %Spacing between elements 
element_width = 0.27/1000;
focus = [0 0 10000]./1000; %mm
wvlToMm = 1.54/(f0*10^(-6)); % Conversion factor from wvlToMm f0 must be in MHz

set_field('fs',fs);

aperture = xdc_linear_array (128,element_width, element_height, kerf, 1, 1, focus);
receiveAperture = xdc_linear_array (128,element_width, element_height, kerf, 1, 1, focus);
impulse_response = sin(2*pi*f0*(0:1/fs:2/f0));

impulse_response=impulse_response.*hanning(max(size(impulse_response)))';
xdc_impulse (aperture, impulse_response);

excitation=sin(2*pi*f0*(0:1/fs:2/f0));
xdc_excitation (aperture, excitation);

xdc_impulse(receiveAperture, impulse_response);

spacing = repmat(0:0.3:127*0.3, 9, 1);
steerAngles = linspace(-8*pi/180, 8*pi/180, 9);
gradient = repmat(tan(steerAngles)',1,128);
%upperHalfDelays = gradient(1:5,:).*spacing(1:5,:) + abs(min(gradient(1:5,:).*spacing(1:5,:), [], 2));
%lowerHalfDelays = gradient(6:9,:).*spacing(6:9,:) ;
delays = gradient.*spacing./1540e3;
%xdc_times_focus(aperture, (0:1/9000:8*1/9000)', delays);

points = [0 0 20; 0 0 10]./1000;
amp = [5; 5];

ensembleLength = 1;
acquisitionLength = 9;
startAngle = -16*(pi/180)/2;
dtheta = 16*(pi/180)/(acquisitionLength-1);

for i = 1:ensembleLength
    steerAngle = startAngle;
    for j = 1:acquisitionLength
        %xdc_focus(aperture, 0, [10*sin(steerAngle) 0 10*cos(steerAngle)]); 
        %xdc_dynamic_focus(receiveAperture, 0, steerAngle, 0);
        xdc_times_focus(aperture, 0, delays(j,:));
        [v,t1] = calc_scat_multi(aperture, aperture, points,amp);
        RcvData{i,j} = v;
        times{i, j} = t1;
        steerAngle = steerAngle + dtheta;
    end
end

