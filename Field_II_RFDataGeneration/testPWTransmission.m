%% Coherent Plane Wave Transmission
% Simulation of coherent plane wave transmissions of a vessel angled at 45
% degrees to the beam. Transducer is modelled after a Verasonics L11-4v
% linear array transducer. 

close all;
clear all;

%% Create Transmit and Receive Apertures
f0 = 6.25e6; %6.25 MHz
fs = 25e6;   %25 MHz
c  = 1540;   % 1540 m/s
lambda = c/f0; % wavelength
element_height = 5/1000; % (m)
kerf = 0.03/1000; %Spacing between elements 
focus = [0 0 100000]./1000; %mm
wvlToMm = 1.54/(f0*10^(-6)); % Conversion factor from wvlToMm f0 must be in MHz

set_field('fs',fs);

aperture = xdc_linear_array (128,0.27/1000, element_height, kerf, 1, 1, focus);

impulse_response = sin(2*pi*f0*(0:1/fs:2/f0));
impulse_response=impulse_response.*hanning(max(size(impulse_response)))';
xdc_impulse (aperture, impulse_response);

excitation=sin(2*pi*f0*(0:1/fs:2/f0));
xdc_excitation (aperture, excitation);

receiveAperture = xdc_linear_array (128,0.27/1000, element_height, kerf, 1, 1, focus);
xdc_impulse(receiveAperture, impulse_response);
xdc_apodization(receiveAperture,0,hann(128)');

%% Generate Phantom Model
[scatterers, scatAmps] = arteryPhantom45deg(20);


%% Acquire RF Data
ensembleLength = 20;
acquisitionLength = 9;
startAngle = -16*(pi/180)/2;
dtheta = 16*(pi/180)/(acquisitionLength-1);
fprf = 9000;
Tprf = 1/fprf;
delays = zeros(9,129);
for i = 1:ensembleLength
    steerAngle = startAngle;
    for j = 1:acquisitionLength
        xdc_focus(aperture, 0, [100*sin(steerAngle) 0 100*cos(steerAngle)]); 
        xdc_focus(receiveAperture, 0, [100*sin(steerAngle) 0 100*cos(steerAngle)]); 
        k = (i-1)*acquisitionLength + j;
        [v,t1] = calc_scat_multi(aperture, receiveAperture, scatterers{k},scatAmps{k});
        delays(j,:) = xdc_get(aperture,'focus');
        t0 = round(t1*fs);
        pulseLength = length(excitation) + 2*length(impulse_response) - 2;
        t0 = round(t0-pulseLength/2);
        rfdata = [zeros(t0,size(v,2));v];
        RcvData{i,j} = rfdata;
        times{i,j} = t1;
        steerAngle = steerAngle + dtheta;
    end
end

%% Free aperture memory
xdc_free(aperture);
xdc_free(receiveAperture);

%% Save RcvData
% Naming Convention: ReceiveData{Ensemble Length}L{Max Blood
% Velocity}b{max tissue velocity}t{comment}.mat'
save('./GeneratedRFData/ReceivedData20L5b3tHanningApod.mat','RcvData','delays');
