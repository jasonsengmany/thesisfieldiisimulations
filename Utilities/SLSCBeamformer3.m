function slscMetric = SLSCBeamformer3(delayedRFData, M, kernel)    
% SLSCBeamformer performs short-lag spatial coherent beamforming on the
% time-delayed channel data with M lags. It is assumed that the
% time-delayed data has been high-pass filtered if it is to be used for
% CFPD.
% Input Args:
%       delayedRFData(t,n,page,optional: scanline) :matrix storing the
%       time-delayed RFsignals for scan-lines from each receive channel
%       M : Maximum lag used for SLSC beamforming.
%           M/NumReceiveChannels ~ 15-30%
%       kernel: number of depth samples used for spatial coherence 
%              calculation
delayedRFDataSquared = movsum(delayedRFData.^2,kernel);
slscMetric = zeros(size(delayedRFData,1), 1, size(delayedRFData,3), size(delayedRFData,4));

for m = 1:M
    dotProds = movsum(delayedRFData.*circshift(delayedRFData,-m,2),kernel);
    normFactor = sqrt(delayedRFDataSquared.*circshift(delayedRFDataSquared,-m,2));
    spatCorr = dotProds./normFactor;
    spatCorr(:,end-m+1:end,:) = 0;
    slscMetric = slscMetric + 1/(size(delayedRFData,2)-m).*sum(spatCorr,2,'omitnan');
end

end

