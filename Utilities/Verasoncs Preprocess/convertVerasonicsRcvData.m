function [RFData,delays] = convertVerasonicsRcvData(RcvData,Receive,TX, Trans)
% convertVerasonicsRcvData pre-processes RFData obtained by the verasonics
% to be processed using the utility functions.
% Input args:
%   RcvData: Variable containing raw RFData which can be saved from the VSX
%   window.
%   Receive: Object defined within the Verasonics set-up scripts which
%   provides sample boundaries and other US parameters.
%   TX:      Object defined within the Verasonics set-up scripts which
%   provides data on transmission event.
for i = 1:length(Receive)
    RFData{Receive(i).framenum,Receive(i).acqNum} = ...
        RcvData{1}(Receive(i).startSample:Receive(i).endSample,:,Receive(i).framenum);
end

delays = zeros(length(TX), length(TX(1).Delay)+1);
% for i = 1:length(TX)
%     delays(i,2:end) = TX(i).Delay;
% end
% wvlToMm = 1.54/Receive(1).demodFrequency;
% c = 1540*10^3;   %speed of sound (mm/s)
% %Convert delay time from wvl to s.
% delays = delays.*wvlToMm/c; 
end
