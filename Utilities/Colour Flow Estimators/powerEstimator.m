function power = powerEstimator(beamformedData, rangeGate)
    power = movsum(sum(real(beamformedData).^2,3) + sum(imag(beamformedData).^2,3), rangeGate);
end