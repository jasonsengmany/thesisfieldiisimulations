function velocity = velocityEstimator1D(IQData, fprf, Fs, F0)
    Ts = 1/fprf;
    ts = 1/Fs;
    c  = 1540;
    fdem = F0/Fs;
    
    M = 20;
    
    Q = imag(IQData);
    Qavg = movsum(Q,M);
    I = real(IQData);
    Iavg = movsum(I,M);
    
    A = sum(Qavg(:,:,1:end-1).*Iavg(:,:,2:end) - Iavg(:,:,1:end-1).*Qavg(:,:,2:end),3);
    B = sum(Iavg(:,:,1:end-1).*Iavg(:,:,2:end) + Qavg(:,:,1:end-1).*Qavg(:,:,2:end),3);
    velocity = c/2*(1/(2*pi*Ts)).*atan(A/B)./(fdem/ts);
end
