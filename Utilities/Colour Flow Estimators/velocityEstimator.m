function velocity = velocityEstimator(IQData, fprf, Fs, F0)
% velocityEstimator Uses the 2D Loupas Autocorrelator to estimate the mean
% velocity across the beamformed IQData. Refer to "An Axial Velocity Estimator for Ultrasound
% Blood Flow Imaging, Based on a Full Evaluation of the Doppler Equation by Means of a
% Two-Dimensional Autocorrelation Approach" by Loupas et al.
% Input args
% fprf : Pusle repetition frequency (Hz)
% Fs   : Sampling frequency (Hz)
% F0   : Centre frequency (Hz)
% Output args
% velocity : Axial velocity mapping along the depth axis of the image.

Ts = 1/fprf;
ts = 1/Fs;
c  = 1540;
fdem = F0/Fs;


M = 8; % Depth range gate.
N = size(IQData, 3);

I = real(IQData);
Q = imag(IQData);

A = movsum(sum(Q(:,:,1:N-1).*I(:,:,2:N) - I(:,:,1:N-1).*Q(:,:,2:N),3),M);
B = movsum(sum(I(:,:,1:N-1).*I(:,:,2:N) + Q(:,:,1:N-1).*Q(:,:,2:N),3),M);

C = movsum(sum(Q(1:end-1,:,:).*I(2:end,:,:) - I(1:end-1,:,:).*Q(2:end,:,:),3),M-1);
D = movsum(sum(I(1:end-1,:,:).*I(2:end,:,:) + Q(1:end-1,:,:).*Q(2:end,:,:),3),M-1);
velocity = c/2*(((1/(2*pi*Ts).*atan2(A(1:end-1,:,:),B(1:end-1,:,:))))./((1/(2*pi*ts)).*((2*pi*fdem)+atan2(C,D))));

end
