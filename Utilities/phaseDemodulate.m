%function phaseDemod = phaseDemodulate(IQData, fprf, Fs,F0)
% Refer to "Adaptive Clutter Demodulation for Non-Contrast Ultrasound 
% Perfusion Imaging" by Tierney et al.
% Define imaging parameters
    fprf = 1000;
    Fs = 25e6;
    F0 = 6.25e6;
    relativeDisplacements = zeros(size(IQData(325:end,:,:),1), size(IQData(325:end,:,:),2));
    rDisplacements = zeros(size(IQData(325:end,:,:),1), size(IQData(325:end,:,:),2));
    i = 2;
    % Calculate relative displacements of adjacent pages
    for page = 1:size(IQData(325:end,:,:),3)-1
        v = velocityEstimatorNonVectorised(IQData(325:end,:,page:page+1),fprf,Fs,F0);
        displacement = v.*(1/fprf);
        relativeDisplacements(:,:,i) = displacement;
        rDisplacements(:,:,page+1) = displacement;
        relativeDisplacements(:,:,i+1) = -1.*displacement;
        i = i + 2;
    end
    
    % Calculate absolute displacements
    absDisplacements = cumsum(rDisplacements,3,'omitnan');
    phaseDemod = zeros(size(absDisplacements));
    zDist =   [(1/ultrasoundParams.fs)*c:(1/ultrasoundParams.fs)*c:ultrasoundParams.endDepth]';
    zDist = zDist(325:end);
    % Perform phase demodulation by using a cubic-piecewise interpolation
    % from the absolute displacements to the original positions in the
    % first page
    for page = 1:size(absDisplacements,3)
        for scanLine = 1:size(absDisplacements,2)
            phaseDemod(:,scanLine,page) = pchip(zDist(1:end) + absDisplacements(:,scanLine,page),...
                IQData(325:end,scanLine,page),zDist(1:end) + absDisplacements(:,scanLine,1));
        end
    end
    
    % Perform amplitude demodulation
    adaptiveDemod = phaseDemod;
    %adaptiveDemod = amplitudeDemodulate(phaseDemod,55); 
    %adaptiveDemod(isnan(adaptiveDemod)) = 0;
     
    