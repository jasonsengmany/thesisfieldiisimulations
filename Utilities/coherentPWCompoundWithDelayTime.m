%function [outputArg1,outputArg2] = coherentPWCompound(channelData,startTimes, ultrasoundParams)
%% Coherent PW Compounding of all frames in the ensemble
% Args:
%   channelData{frame,acquisition}
%   startTimes [a
% All units are to be m, Hz, s
c = 1540;
channelData = RcvData;
startTimes = times;
ultrasoundParams = struct('pitch',0.3/1000,...
                          'fs',100e6,...
                          'endDepth', 25/1000);
                      
transmit = struct('numAcquisitions', 9,...
                  'steerAngle', linspace(-8*pi/180,8*pi/180,9));
              
% Initialise the coordinate system for image reconstruction.
[xCoords, zCoords] = meshgrid([0:ultrasoundParams.pitch:127*ultrasoundParams.pitch],...
    [(1/ultrasoundParams.fs)*c:(1/ultrasoundParams.fs)*c:ultrasoundParams.endDepth]);

timeToScat = zCoords.*1540;

ccData = zeros(size(zCoords));
for acquisition = 1:9
    dasRFData = zeros(size(zCoords));
    for scanLine = 0:(size(xCoords,2)-1)
        deltaX = xCoords - scanLine*ultrasoundParams.pitch;
        timeToEle = sqrt(zCoords.^2 + deltaX.^2)./c;
        totalTime = timeToScat +  timeToEle - delays(acquisition,scanLine+2);
        queryTime = totalTime - startTimes{1,acquisition};
        rowIndexes = round(queryTime.*ultrasoundParams.fs);
        rowIndexes(rowIndexes <= 0) = 1;
        rowIndexes(rowIndexes >= size(channelData{1,acquisition},1)) = 1;
        colIndexes = repmat(1:128,size(rowIndexes, 1), 1);
        indexes = sub2ind(size(channelData{1,acquisition}),rowIndexes,colIndexes);
        delayedVal = channelData{1,acquisition}(indexes);
        dasRFData(:,scanLine+1) = sum(delayedVal,2);
    end    
    ccData = ccData + dasRFData;
end


imagesc(abs(hilbert(ccData)));
colormap('gray');


