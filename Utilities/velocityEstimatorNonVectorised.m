function velocity = velocityEstimatorNonVectorised(IQData, fprf, Fs, F0)
Ts = 1/fprf;
ts = 1/Fs;
c  = 1540;
fdem = F0/Fs;

M = 8;
N = size(IQData, 3);

I = permute(real(IQData), [1 3 2]);
Q = permute(imag(IQData), [1 3 2]);

velocity = zeros(size(IQData,1), size(IQData,2));

for scanLine = 1:size(IQData,2)
    Is = I(:,:,scanLine);
    Qs = Q(:,:,scanLine);
    for row = 1:size(IQData,1)
        A = 0;
        B = 0;
        for m = row:min(row+M,size(IQData,1))
            for n = 1:N-1
                A = A + Qs(m,n).*Is(m,n+1) - Is(m,n).*Qs(m,n+1);
                B = B + Is(m,n).*Is(m,n+1) + Qs(m,n).*Qs(m,n+1);
            end
        end
        
        C = 0;
        D = 0;
        for m = row:min(row+M-1,size(IQData,1)-1)
            for n = 1:N
                C = C + Qs(m,n).*Is(m+1,n) - Is(m,n).*Qs(m+1,n);
                D = D + Is(m,n).*Is(m+1,n) + Qs(m,n).*Qs(m+1,n);
            end
        end
        v = c/2*(1/(2*pi*Ts).*atan2(A,B))./(1/(2*pi*ts)*(2*pi*fdem + atan2(C,D)));
        velocity(row,scanLine) = v;
    end
end