function slscMetric = SLSCBeamformer(delayedRFData, M)    
% SLSCBeamformer performs short-lag spatial coherent beamforming on the
% time-delayed channel data with M lags. It is assumed that the
% time-delayed data has been high-pass filtered if it is to be used for
% CFPD.
% Input Args:
%       delayedRFData(t,n,page,optional: scanline) :matrix storing the
%       time-delayed RFsignals for scan-lines from each receive channel
%       M : Maximum lag used for SLSC beamforming.
%           M/NumReceiveChannels ~ 15-30%
delayedRFDataSquared = delayedRFData.^2;
delayedRFDataSquared = movsum(delayedRFDataSquared,4);
slscMetric = zeros(size(delayedRFData,1), 1, size(delayedRFData,3), size(delayedRFData,4));
for m = 1:M
    dotProds = delayedRFData(:,1:end-m,:,:).*delayedRFData(:,1+m:end,:,:);
    dotProdsKernAvg = movsum(dotProds,4);
    refSignalSquaredAvg = delayedRFDataSquared(:,1:end-m,:,:);
    lagSignalSquaredAvg = delayedRFDataSquared(:,1+m:end,:,:);
    normFactor = sqrt(refSignalSquaredAvg.*lagSignalSquaredAvg);
    %Create a mask to remove zero division errors
    %normFactor(normFactor == 0) = 1;
    spatCorr = dotProdsKernAvg./normFactor;
    slscMetric = slscMetric + 1/(128-m).*sum(spatCorr,2,'omitnan');
end

end
%% Vectorized version runs slower
% refSignal = zeros([size(delayedRFData) M]);
% lagSignal = zeros([size(delayedRFData) M]);
% lags = zeros(size(delayedRFData,1),1,size(delayedRFData,3),M);
% repDelayedRFDataSquared = repDelayedRFData.^2;
% for m = 1:M
%    refSignal(:,1:end-m,:,m) = delayedRFData(:,1:end-m,:);
%    lagSignal(:,1:end-m,:,m) = delayedRFData(:,1+m:end,:);
%    lags(:,:,:,m) = m;
% end
% 
% dotProds = movsum(refSignal.*lagSignal, 4,1);
% refSignalSquared = movsum(refSignal.^2,4,1);
% lagSignalSquared = movsum(lagSignal.^2,4,1);
% normFactor = sqrt(refSignalSquared.*lagSignalSquared);
% indexes = find(normFactor == 0);
% normFactor(indexes) = normFactor(indexes) + 1;
% spatCorr = 1/(128-lags).*sum(dotProds./normFactor,2);
% slscMetric = sum(spatCorr,4);

%end