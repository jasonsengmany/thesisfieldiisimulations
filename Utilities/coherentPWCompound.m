%function [outputArg1,outputArg2] = coherentPWCompound(channelData,startTimes, ultrasoundParams)
%% Coherent PW Compounding of all frames in the ensemble
% Args:
%   channelData{frame,acquisition}
%   startTimes [a
% All units are to be m, Hz, s
c = 1540;
channelData = RcvData;
ultrasoundParams = struct('pitch',0.3/1000,...
                          'fs',25e6,...
                          'endDepth', 30/1000,...
                          'ensembleLength', 20);
                      
transmit = struct('numAcquisitions', 9,...
                  'steerAngle', linspace(-8*pi/180,8*pi/180,9),...
                  'delays', delays);
              
% Initialise the coordinate system for image reconstruction.
[xCoords, zCoords] = meshgrid([0:ultrasoundParams.pitch:127*ultrasoundParams.pitch],...
    [(1/ultrasoundParams.fs)*c:(1/ultrasoundParams.fs)*c:ultrasoundParams.endDepth]);


beamformedData = zeros([size(zCoords) ultrasoundParams.ensembleLength]);
for page = 1:ultrasoundParams.ensembleLength
    page
    ccData = zeros(size(zCoords));
    for acquisition = 1:9
        timeToScat =  calcAwayTravelTime(zCoords,xCoords-127*ultrasoundParams.pitch/2,transmit.steerAngle(acquisition));
        dasRFData = zeros(size(zCoords));
        for scanLine = 1:size(xCoords,2)
            timeToEle = calcToTravelTime(zCoords, xCoords - (scanLine-1)*ultrasoundParams.pitch);
            timeToScatInScanLine = timeToScat(:,scanLine);
            totalTime = calcTotalDelayTime(timeToScatInScanLine, timeToEle, delays(acquisition,2:end));
            queryTime = totalTime; % - startTimes{1,acquisition};
            rowIndexes = round(queryTime.*ultrasoundParams.fs);
            % Replace all indexes outside of the channelData range
            rowIndexes(rowIndexes <= 0) = 1;
            rowIndexes(rowIndexes >= size(channelData{page,acquisition},1)) = 1;
            colIndexes = repmat(1:128,size(rowIndexes, 1), 1);
            indexes = sub2ind(size(channelData{page,acquisition}),rowIndexes,colIndexes);
            delayedVal = channelData{page,acquisition}(indexes);
            dasRFData(:,scanLine) = sum(delayedVal,2);
        end
        ccData = ccData + dasRFData;
    end
    beamformedData(:,:,page) = ccData; 
end

save('BeamformedDataTissueMotion.mat', 'beamformedData');

imagesc(abs(hilbert(ccData)))
colormap('gray');
title('Coherent planewave compounded');

yticklabels = 0:5:ultrasoundParams.endDepth*1000;
yticks = linspace(1,size(ccData,1), numel(yticklabels));

xticklabels = linspace(-128*ultrasoundParams.pitch/2*1000,128*ultrasoundParams.pitch/2*1000,5);
xticks = linspace(1,size(ccData,2), numel(xticklabels));

set(gca, 'YTick', yticks, 'YTickLabel', yticklabels(:),'YMinorTick','on','TickDir', 'out');
set(gca, 'XTick', xticks, 'XTickLabel', xticklabels(:),'XMinorTick','on','TickDir', 'out');

[X,Y] = meshgrid(1:0.1:128, 0:0.1:size(abs(hilbert(ccData)),1));
interpolatedImage = interp2(1:128,1:size(abs(hilbert(ccData)),1), abs(hilbert(ccData)), X, Y, 'cubic');
figure;
imagesc(interpolatedImage);
title('Interpolated DAS Envelope B-Mode');
colormap('gray');
