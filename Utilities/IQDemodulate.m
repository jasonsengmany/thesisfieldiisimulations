function signalIQ = IQDemodulate(RFData, centreFreq, sampleFreq)
% IQDemodulate Performs IQ demodulation on the RFData along each column
    t = (1:size(RFData,1)).*(1/(sampleFreq/2));
    [b,a] = butter(5, centreFreq/(sampleFreq/2));
    signalIQ = RFData.*exp(-1i*2*pi*centreFreq.*t)';
    % filtfilt used to counter phase shift from filtering
    signalIQ = sqrt(2)*filtfilt(b,a,signalIQ);
end
