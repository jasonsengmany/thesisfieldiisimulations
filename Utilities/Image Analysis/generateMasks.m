ultrasoundParams = struct('pitch',0.3/1000,...
                          'fs',2*25e6,...
                          'endDepth', 30/1000,...
                          'ensembleLength', 100);             
c = 1540;

[xCoords, zCoords] = meshgrid([0:ultrasoundParams.pitch/2:127*ultrasoundParams.pitch],...
    [(1/ultrasoundParams.fs)*c:(1/ultrasoundParams.fs)*c:ultrasoundParams.endDepth]);
 
insideVessel = zCoords < (-xCoords+0.0140+(25+sqrt(1))/1000) & zCoords > (-xCoords+0.0140+(25-sqrt(1))/1000);
insideVesselMask = insideVessel & (xCoords > 0.014) & (xCoords < 0.0240) & (zCoords > 0.010) & (zCoords < 0.03);
imagesc(insideVesselMask);
insideVessel = zCoords < (-xCoords+0.0140+(25+sqrt(4))/1000) & zCoords > (-xCoords+0.0140+(25-sqrt(4))/1000);
backgroundMask = ~insideVessel & (xCoords > 0.0140) & (xCoords < 0.0240) & (zCoords > 0.010) & (zCoords < 0.03);
%backgroundMask(1:50,:) = 0;
%backgroundMask(end-50:end,:) = 0;
figure;
imagesc(backgroundMask);
save('FTTAnalysisMasks2xAxialRes.mat', 'insideVesselMask','backgroundMask');