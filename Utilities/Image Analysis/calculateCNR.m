function cnr = calculateCNR(USImage,signalMask, bkgMask)
%CALCULATECNR Summary of this function goes here
%   Detailed explanation goes here
bloodSignal = 1/length(signalMask(signalMask == 1))*sum(USImage.*signalMask,'all');
bkgSignal = 1/length(bkgMask(bkgMask == 1))*sum(USImage.*bkgMask,'all');
cnr = 10*log10(abs(bloodSignal-bkgSignal)/std(USImage(bkgMask ~= 0)));
end

