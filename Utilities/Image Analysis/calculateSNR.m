function snr = calculateSNR(USImage,signalMask, bkgMask)
%CALCULATESNR Summary of this function goes here
%   Detailed explanation goes here
signalIntensity = 1/length(signalMask(signalMask == 1))*sum((USImage.*signalMask).^2,'all');
bkgIntensity = 1/length(bkgMask(bkgMask == 1))*sum((USImage.*bkgMask).^2,'all');
snr = 10*log10(sqrt(signalIntensity/bkgIntensity));

end

