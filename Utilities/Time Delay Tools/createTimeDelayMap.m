function [rowIndexes,colIndexes,apertureRanges] = createTimeDelayMap(xCoords,zCoords,receiveChannelCoords,transmit,ultrasoundParams, F)
%CREATETIMEDELAYMAP Creates a mapping of row and column indexes for each
% xCoord and zCoord position within the scanning field for each steer angle.
% Top left corner/left-most element in transducer taken as origin (0,0)
% Input Args:
%       xCoords(m,n) and zCoords(m,n): meshgrid of the points for
%       reconstruction/beamforming to take place.
%       receiveChannelCoords(m,i): x-coordinates of corresponding
%       transducer element, i, replicated for all depth samples m.
%       transmit: struct{numAcquisiitons, steerAngles, delays}
%       ultrasoundParams: struct{pitch, fs, endDepth, ensembleLength}
%       F: F# number (depth/receive aperture)
%
% Output Args:
%       rowIndexes/colIndexes(x,y,scanline,acq): Mapping of the rcvData row
%       and column indexes for each scanline and each acq. 
%       apertureRanges:
%       Consists of the active receive channels associated with each
%       scanline

deltaX = xCoords(1,2)-xCoords(1,1);

a = ultrasoundParams.endDepth/(2*F);
receiveChannels = receiveChannelCoords(1,:);
for scanLine = 1:size(xCoords,2)
    apertureRanges{scanLine} = find(receiveChannels >= (scanLine-1)*deltaX-a & ...
         receiveChannels <= (scanLine-1)*deltaX+a);
end
    


rowIndexes = zeros([size(receiveChannelCoords) size(xCoords,2) transmit.numAcquisitions]);
colIndexes = zeros([size(receiveChannelCoords) size(xCoords,2) transmit.numAcquisitions]);
for acquisition = 1:transmit.numAcquisitions
    %Calculate time for the ultrasound to reach the scatterers within
    %scanning region (xCoords,zCoords)
    timeToScat = calcAwayTravelTime(zCoords,xCoords,transmit.steerAngle(acquisition));
    for scanLine = 1:size(xCoords,2)
        timeToEle = calcToTravelTime(zCoords(:,apertureRanges{scanLine}), ...
            receiveChannelCoords(:,apertureRanges{scanLine}) - (scanLine-1)*abs(deltaX));
        timeToScatInScanLine = timeToScat(:,scanLine);
        totalTime = calcTotalDelayTime(timeToScatInScanLine, timeToEle, transmit.delays(acquisition,apertureRanges{scanLine}+1));
        rowIndexes(:,apertureRanges{scanLine},scanLine,acquisition) = floor(totalTime.*ultrasoundParams.fs);
        colIndexes(:,:,scanLine,acquisition) = repmat(1:128,size(rowIndexes, 1), 1);
    end
end

end

