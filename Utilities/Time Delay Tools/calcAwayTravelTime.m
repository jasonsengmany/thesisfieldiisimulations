function times = calcAwayTravelTime(zCoords,xCoords, steerAngle)
% calcAwayTravelTime returns the time taken to reach a scatterer in
% position xCoords,zCoords with the given steerAngle.
if steerAngle >= 0
    times = (zCoords.*cos(steerAngle) + xCoords.*sin(steerAngle))./1540;
else
    %Note: Due to the  pivot of the steer for Verasonics transmissions to
    %be at either ends of the transducer, a seperate case is created for
    %negative steer angles (i.e, with pivot on right most element of the
    %transducer).
    times = (zCoords.*cos(-steerAngle) + xCoords.*sin(-steerAngle))./1540;
    times = flip(times,2);
end

end

