function totalDelayTime = calcTotalDelayTime(timeAway,timeTo,delay)
%calcTotalDelayTime 
%   timeAway: [n x m] matrix for the travel time to each pixel point
%             coordinate.
%   timeTo  : [n x m] matrix for the travel time from each pixel point to the
%             transducer element in the scanLine being considered.
%   delay   : [1 x m] vector consisting of the focussing delay times for
%             each transducer element.
totalDelayTime = timeAway + timeTo + delay;
end

