function time = calcToTravelTime(zCoords,xDist)
%calcToTravelTime
%   Creates a mapping for the return times of ultrasound from each pixel
%   location to a specific scanline. Assumes speed of sound in tissue is
%   1540m/s. All units to be given in m.
%   Input Args:
%       zCoords: 2D matrix with zCoordinates. i.e. each row will have the
%       same value.
%       xDist:   distance between the receive channel and the x-coordinate
%       of the pixels.
time = sqrt(zCoords.^2 + xDist.^2)./1540;
end

