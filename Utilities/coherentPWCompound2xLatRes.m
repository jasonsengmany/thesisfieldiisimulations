function beamformedData = coherentPWCompound2xLatRes(RcvData,ultrasoundParams, transmit, xCoords, zCoords)
% coherentPWCompound2xLatRes beamforms raw data using coherent plane wave
% compounding described in "Coherent Plane-Wave Compounding for Very High Frame Rate
% Ultrasonography and Transient Elastography" by Montaldo.
% Input Args:
%   RcvData{m,n}: Cell array storing RFData for all receive channels 
%                 page, m, and acquisition ,n. Generated in Field II or
%                 Verasonics.
%   ultrasoundParams: Struct consisting of the following fields...
%                       struct('pitch',0.3/1000,...
%                          'fs',25e6,...
%                          'f0',6.25e6,...
%                          'dopPRF',1000,...
%                          'endDepth', 30/1000,...
%                          'ensembleLength', 20);
%   transmit: Struct consisting of the following fields...
%               struct('numAcquisitions', 9,...
%                   'steerAngle', linspace(-8*pi/180,8*pi/180,9),...
%                   'delays', delays);
%   xCoords,zCoords: meshgrid consisting of coordinates for the image
%                       reconstruction pixel locations.
%  All units are to be m, Hz, s


receiveChannelCoords = repmat(0:ultrasoundParams.pitch:127*ultrasoundParams.pitch,...
                                size(zCoords,1),1);

%% Create time-delay mapping
% Note: xCoord origin is taken as centre of transducer for Field II
% Reconstruction.
[rowIndexes, colIndexes] = createTimeDelayMap(xCoords,zCoords,receiveChannelCoords,transmit,ultrasoundParams, 1.5);
rowIndexes(rowIndexes <= 0) = 1;
                  
beamformedData = zeros([size(zCoords) ultrasoundParams.ensembleLength]);
for page = 1:ultrasoundParams.ensembleLength
    ccData = zeros(size(zCoords));
    fprintf("Page:%d ", page);
    tic
    for acquisition = 1:transmit.numAcquisitions
        dasRFData = zeros(size(zCoords));
        for scanLine = 1:size(xCoords,2)
            % Replace all indexes outside of the channelData range
            [r,c] = find(rowIndexes(:,:,scanLine,acquisition) >= ...
                size(RcvData{page,acquisition},1));
            rowIndexes(r,c,scanLine,acquisition) = 1;

            indexes = sub2ind(size(RcvData{page,acquisition}),rowIndexes(:,:,scanLine, acquisition),colIndexes(:,:,scanLine,acquisition));
            delayedVal = RcvData{page,acquisition}(indexes);
            dasRFData(:,scanLine) = sum(delayedVal,2);
        end
        ccData = ccData + dasRFData;
    end
    beamformedData(:,:,page) = ccData; 
    toc
end

% Display B-Mode image
imagesc(abs(hilbert(ccData)))
colormap('gray');

end