function slscMetric = SLSCBeamformer2(delayedRFData, M)    
% SLSCBeamformer performs short-lag spatial coherent beamforming on the
% time-delayed channel data with M lags. It is assumed that the
% time-delayed data has been high-pass filtered if it is to be used for
% CFPD.
% Input Args:
%       delayedRFData(t,n,page,optional: scanline) :matrix storing the
%       time-delayed RFsignals for scan-lines from each receive channel
%       M : Maximum lag used for SLSC beamforming.
%           M/NumReceiveChannels ~ 15-30%

delayedRFData = permute(delayedRFData,[2 1 3 4]);
delayedRFDataSquared = delayedRFData.^2;
delayedRFDataSquared = movsum(delayedRFDataSquared,4,2);


delayedRFData = num2cell(delayedRFData,1);
dotProds = cellfun(@(x) xcorr(x,M), delayedRFData, 'UniformOutput',false);
dotProds = cell2mat(dotProds);
dotProds = dotProds(end-M+1:end,:,:);
dotProds = movsum(dotProds,4,2);

delayedRFDataSquared = num2cell(delayedRFDataSquared,1);
normFactor = cellfun(@(x) xcorr(x,M), delayedRFDataSquared, 'UniformOutput',false);
normFactor = cell2mat(normFactor);
normFactor = normFactor(end-M+1:end,:,:);
normFactor(normFactor == 0) = 1;
lags = repmat((1:M)',1,size(dotProds,2),size(dotProds,3));
spatCorr = 1/(128-lags).*(dotProds./sqrt(normFactor));
slscMetric = permute(spatCorr,[2 1 3 4]);
slscMetric = sum(slscMetric,2);
end

