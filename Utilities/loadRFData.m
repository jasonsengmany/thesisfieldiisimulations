
%Adjust path as required
path = './GeneratedRFData/Case25/';

for i = 1:100
    for j = 1:9
        load([path 'page' num2str(i) 'acquisition' num2str(j) '.mat']);
        RcvData{i,j} = rfdata;
    end
end
RcvData = cellfun(@(x) resample(x,2,1),RcvData,'UniformOutput',false);
load([path 'delays.mat']);