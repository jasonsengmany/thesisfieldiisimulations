function [f0,power0] = analyseFreqSpectrum(data, fs)
    n = length(data);
    f0 = (-n/2:n/2-1)*(fs/n);
    y0 = (fftshift(fft(data)));
    power0 = 20*log10(sqrt(abs(y0).^2/n))';
%     plot(f0,power0);
%     xlabel('Frequency');
%     ylabel('Power');
end