function IQData = IQSample(beamformedData, centreFreq, sampleFreq)

T = 1/centreFreq;
Ts = 1/sampleFreq;
n = linspace(1,(size(beamformedData,1).*Ts-T/4)/T,size(beamformedData,1));
sampleTimes = round(n.*T/Ts);
x = beamformedData(sampleTimes,:,:);
sampleTimes = round((n.*T + T/4)/(1/50e6));
y = beamformedData(sampleTimes,:,:);

IQData = x + 1i*y;
end
