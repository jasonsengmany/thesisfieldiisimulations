function A = butterProjectionInitialised(order,Wn,numSamples)
%BUTTERPROJECTIONINITIALISED Creates a projection-initialised butterworth
% filter reduced to a single matrix.
% Input Args:
%           order and Wn: output arguments from the 'buttord' function
[F,q,gT,d] = butter(order,Wn,'high');

B = zeros(numSamples ,order);
C = diag(d*ones(1,numSamples));
for i = 1:(numSamples)
    B(i,:) = gT*F^(i-1);
    if i > 1
        for j = (i-1):-1:1
            C(i,j) = gT*F^(i-j-1)*q;
        end
    end
end

A = (eye(length(B))-B*(B'*B)^(-1)*B')*C;
end
