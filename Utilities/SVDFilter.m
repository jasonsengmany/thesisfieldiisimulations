function [filteredBeamformedData,V,D] = SVDFilter(beamformedData, lowerThreshold, upperThreshold)
% SVDFilter Uses spatio-temporal filtering to remove tissue clutter. Refer
% to "Spatiotemporal Clutter Filtering of Ultrafast Ultrasound Data Highly 
% Increases Doppler and fUltrasound Sensitivity" by Demene et al.
% Input Args:
%   beamformedData(m,n,p): Set of beamformed data with depth,m, column, n and
%                           page, p.
%   upper/lower threshold : Value within the range of number of pages of beamformed data
%             to determine which singular values and vectors to use during filtering. A
%             higher threshold will maintain a greater amount of the lower end singular
%             values.
% Output Args:
%   fiilteredBeamformedData: SVD filtered data
%   V: singulat vector representing the set of temporal vectors in which 
%       the ultrasound image is projected onto.
%   D: diagonal matrix contain singular values.    
filteredBeamformedData = zeros(size(beamformedData));
casoratiMatrix = createCasoratiMatrix(beamformedData);
covarianceMatrix = cov(casoratiMatrix);
[V,D] = eig(covarianceMatrix);
I = eye(size(D));
I(:,upperThreshold:end) = 0;
I(:,1:lowerThreshold) = 0;
filteredCasoratiMatrix = casoratiMatrix*V*I*V';

for page = 1:size(beamformedData,3)
   filteredBeamformedData(:,:,page) = reshape(filteredCasoratiMatrix(:,page),...
       [size(beamformedData,1) size(beamformedData,2)]);
end

end

