close all;
depth1 = 225*2;
depth2 = 325*2;
IQSig = IQDemodulate(beamformedData, 6.25e6, 25e6);
%IQSig = adaptiveDemod;
rearrangedIQSig = permute(IQSig, [3,1,2]);
[b,a] = butter(2,25/100,'high');
analyseFreqSpectrum(abs(rearrangedIQSig(:,depth1,127)),1000);
title('Frequency Spectrum Analysis');
hold on;
analyseFreqSpectrum(abs(rearrangedIQSig(:,depth2,127)),1000);
hold off;

figure; plot(abs(rearrangedIQSig(:,depth1,127)));
hold on;
plot(abs(rearrangedIQSig(:,depth2,127)));
title('Plot of stationary tissue and blood RFData across Ensemble');
hold off;

filteredTissueData = filter(b,a,rearrangedIQSig(:,depth1,127));
filteredBloodData = filter(b,a,rearrangedIQSig(:,depth2,127));
figure;
plot(abs(filteredTissueData));
hold on;
plot(abs(filteredBloodData));
hold off;

filteredSignal = filter(b,a,IQSig,[],3);
power = powerEstimator(filteredSignal(340:end-20,:,15:end));
power = medfilt2(power,[5 5]);
figure;
imagesc(power);
title('PDop Image IIR Filt 100Hz LFC');
colormap('hot');