function casoratiMatrix = createCasoratiMatrix(beamformedData)
%CREATECASORATIMATRIX reshapes the beamformedData into a Casorati Matrix
casoratiMatrix = zeros(size(beamformedData,1)*size(beamformedData,2),size(beamformedData,3));
for page = 1:size(beamformedData,3)
    casoratiMatrix(:,page) = reshape(beamformedData(:,:,page),...
        [size(beamformedData,1)*size(beamformedData,2) 1]);
end

end

