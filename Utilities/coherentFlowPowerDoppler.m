function [power, beamformedData] = coherentFlowPowerDoppler(RcvData,ultrasoundParams, transmit, xCoords, zCoords,lfc)
% coherentFlowPowerDoppler generates a coherent flow power doppler image
% based off theory presented in "Visualization of Small-Diameter Vessels by 
% Reduction of Incoherent Reverberation With Coherent Flow Power Doppler"
% by Dahl .J.
% Input Args:
%   RcvData{m,n}: Cell array storing RFData for all receive channels 
%                 page, m, and acquisition ,n. Generated in Field II or
%                 Verasonics.
%   ultrasoundParams: Struct consisting of the following fields...
%                       struct('pitch',0.3/1000,...
%                          'fs',25e6,...
%                          'f0',6.25e6,...
%                          'dopPRF',1000,...
%                          'endDepth', 30/1000,...
%                          'ensembleLength', 20);
%   transmit: Struct consisting of the following fields...
%               struct('numAcquisitions', 9,...
%                   'steerAngle', linspace(-8*pi/180,8*pi/180,9),...
%                   'delays', delays);
%   xCoords,zCoords: meshgrid consisting of coordinates for the image
%                       reconstruction pixel locations.
%   lfc: low frequency cut-off for tissue clutter filter 

%  All units are to be m, Hz, s
channelData = RcvData;

%% Wall Filter for clutter filtering
%  Stopband frequency to be adjusted based off LFC
[n,Wn] = buttord(lfc/500,70/500,3,120);
A = butterProjectionInitialised(n,Wn,ultrasoundParams.ensembleLength);

receiveChannelCoords = repmat(0:ultrasoundParams.pitch:127*ultrasoundParams.pitch,...
                                size(zCoords,1),1);

%% Create time-delay mapping 
[rowIndexes, colIndexes, apertureRanges] = createTimeDelayMap(xCoords,zCoords,receiveChannelCoords,transmit,ultrasoundParams,1.5);
rowIndexes(rowIndexes <= 0) = 1;

%% Perform SLSC beamforming
delayedRFData = zeros(size(zCoords,1), 128, ultrasoundParams.ensembleLength);
beamformedData = zeros([size(zCoords) ultrasoundParams.ensembleLength]);
for acquisition = 1:transmit.numAcquisitions
    for scanLine = 1:size(xCoords,2)
        fprintf('Acquisition: %d ScanLine: %d ', acquisition, scanLine);
        tic
        for page = 1:ultrasoundParams.ensembleLength
            indexes = sub2ind(size(channelData{page,acquisition}),rowIndexes(:,:,scanLine, acquisition),colIndexes(:,:,scanLine,acquisition));
            delayedRFData(:,:,page) = channelData{page,acquisition}(indexes);
        end
        % Apply wall filter across ensemble   
        delayedRFData = filterEnsemble(A,delayedRFData);
        M = round(0.15*size(apertureRanges{scanLine},2));
        slscMetric = SLSCBeamformer3(delayedRFData(:,apertureRanges{scanLine},:),M,12);
        beamformedData(:,scanLine,:) = beamformedData(:,scanLine,:) + slscMetric;
        toc
    end
end

%% Calculate power
% Note: No kernel averaging performed due to the SLSC beamformer already
% taking an axial average.
power = sum(beamformedData(:,:,25:100).^2,3);

end

