function amplitudeDemodSignal = amplitudeDemodulate(IQData,kernel)
    power = sqrt(sum(abs(IQData).^2,3)./size(IQData,3));
    normfactor = repmat(power,1,1,size(IQData,3))./abs(IQData);
    normfactor = medfilt1(normfactor, kernel,[],3);
    amplitudeDemodSignal = IQData.*normfactor;
end
