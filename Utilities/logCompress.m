function compressedImg = logCompress(img)
    imgLog = log(img);
    minLog = min(imgLog(:));
    maxLog = max(imgLog(:));
    compressedImg = 255 * (imgLog - minLog)./(maxLog-minLog);
end