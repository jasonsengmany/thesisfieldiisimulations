SNR = zeros(150,30);
CNR = zeros(150,30);
for passband = 10:10:300
    fprintf("Passband:%d\n",passband);
    for stopband = max(1,0.5*passband-20):(0.5*passband)
        tic
        fprintf("Stopband:%d\n",stopband);
        %% Clutter Filtering
        % A butterworth created by first estimating the order and cut-off
        % frequencies before projection initialisation.
        [n,Wn] = buttord(passband/500,stopband/500,3,120);
        A = butterProjectionInitialised(n,Wn,ultrasoundParams.ensembleLength);
        %[b,a] = butter(n,Wn,'high');
        %filteredData = filter(b,a,IQData,[],3);
        filteredData = filterEnsemble(A, IQData);
        
        %% Power estimation
        power = powerEstimator(filteredData(:,:,25:100),9);
        SNR(stopband,passband/10) = calculateSNR(power(441:856,95:161),insideVesselMask(441:856,95:161),backgroundMask(441:856,95:161));
        CNR(stopband,passband/10) = calculateCNR(power(441:856,95:161), insideVesselMask(441:856,95:161),backgroundMask(441:856,95:161));
        toc
    end
    
end
