function [scatterers, scatAmps, within_vessel] = arteryPhantom45deg (ensembleLength)
%% Simulated Scatterer Generation for 45 degree Angled Vessel
% Creates a set of scatterer coordinates and associated amplitudes to model
% a 45 degree angled vessel. Tissue motion is simulated through axial
% displacement of each scatterer. All units are in [m] and [Hz]

    %% Define initial domains for scatterers [m]
    x_range = 0.080;
    y_range = 0.006;
    z_range = 0.080;
    z_depth = 0.020;

    R = 0.001; %Radius of vessel [m]

    %% Calculate number of scatterers to generate. 
    %  20 scatterer per resolution cell

    f0 = 6.25; %Transducer centre frequency;
    lambda = 1540/(f0*10^6);

    N=round(20*x_range/(5*lambda)*y_range/(5*lambda)*z_range/(lambda*2));
    disp([num2str(N),' Scatterers']);

    %% Generate coordinates and amplitudes for scatterers
    randn('seed',sum(100*clock));

    x=x_range*(rand(1,N)-0.5);
    y=y_range*(rand(1,N)-0.5);
    z=z_range*(rand(1,N)-0.5);


    r=(y.^2+z.^2).^0.5;
    within_vessel= (r < R)';
    scatPositionsOutsideVessel = find(r >= R)';
    scatPositionsInsideVessel = find(r < R)';

    v0=0.05; %Largest velocity of scatterers [m/s]
    velocity=v0*(1-(r/R).^2).*within_vessel';
    blood_to_stationary= 0.01; 
    
    amp = randn(N,1).*((1-within_vessel) + within_vessel*blood_to_stationary);
    
    outsideVesselIndexes = find(within_vessel == 0);
    %% Generate the scatterers for the simulation
    %  Each acquisition in the ensemble requires a unique set of scatterer
    %  positions. Scatterer motion is simulated through changes in their
    %  positions between each pulse repetition interval. 
    theta = 45/180*pi;
    c=1540;
    f_max = 2*v0*cos(theta)/c*f0;
    fprintf("Max doppler frequency %d\n", f_max);
    fprf = 9000;
    Tprf = 1/fprf;
    numberOfAcquisitions = 9;
    %tissueFrequency = 0.2;
    maxTissueVelocity = 0.003;
    %Tissue velocities prior to rotating the artery 45 deg
    xzTissueVelocity = sqrt(maxTissueVelocity^2/2); 
    maxNumberOfScatterers = 0;
    
    xnew = x*cos(theta) + z*sin(theta);
    znew = z*cos(theta) - x*sin(theta) + z_depth;
        
    % Crop the scatterers to lie within the imaging region.
    insideXrange = intersect(find(xnew > -0.005),find(xnew < 0.005));
    insideZrange = intersect(find(znew > 0.01), find(znew < 0.03));
    insideScanRegion = intersect(insideXrange, insideZrange);
    
    tissueScattererIndexes = intersect(insideScanRegion,outsideVesselIndexes);
    
    for page = 1:ensembleLength*numberOfAcquisitions

        % Angle all scatterers to lie 45 degrees to horizontal.
        xnew = x*cos(theta) + z*sin(theta);
        znew = z*cos(theta) - x*sin(theta) + z_depth;
        
        insideXrange = intersect(find(xnew > -0.005),find(xnew < 0.005));
        bloodScattererIndexes = intersect(insideXrange, find(within_vessel == 1));
        
        insideScanRegion = union(tissueScattererIndexes,bloodScattererIndexes);
        
        if size(insideScanRegion,2) > maxNumberOfScatterers
            maxNumberOfScatterers = size(insideScanRegion,2);
        end
        
        %Create an offset scatterer so that start times will be the same in
        %all transmit events and the end time will also be the same.
        offset1=[0 0 0.002];
        offset2=[0 0 0.032]; 
        
        positions = [xnew(insideScanRegion); y(insideScanRegion) ; znew(insideScanRegion)]';
        positions = [offset1
                    positions
                    offset2];
                
        ampNow = amp(insideScanRegion);
        ampNow = [0
                  ampNow
                  0];
        
        scatterers{page} = positions;
        scatAmps{page} = ampNow;

        % Shift scatterer positions within the vessel
        x = x + (velocity+xzTissueVelocity)*Tprf;
        z = z - xzTissueVelocity*Tprf;
        outside_range = (x > x_range/2);
        x = x - x_range*outside_range; 
    end

end
