%% CFPD reconstruction sequence using IIR filter
%  Power doppler reconstruction using an SLSC beamformer and a projection-
%  initialised Butterworth filter. Load data the appropriate RFData file 
%  prior to executing this script.

%% Define Ultrasound Parameters
ultrasoundParams = struct('pitch',0.3/1000,...
                          'fs',50e6,...
                          'f0',6.25e6,...
                          'dopPRF',1000,...
                          'endDepth', 30/1000,...
                          'ensembleLength', 100);
                      
transmit = struct('numAcquisitions', 9,...
                  'steerAngle', linspace(-8*pi/180,8*pi/180,9),...
                  'delays', delays);
c = 1540;

%% Initialise the coordinate system for image reconstruction.
[xCoords, zCoords] = meshgrid([0:ultrasoundParams.pitch/2:127*ultrasoundParams.pitch],...
    [(1/ultrasoundParams.fs)*c:(1/ultrasoundParams.fs)*c:ultrasoundParams.endDepth]);

% Note: xCoord origin is taking at the centre of the transducer for Field
% II generated data as the steering is pivoted around this centre point.
xCoords = xCoords-127*ultrasoundParams.pitch/2;

%% Perform CFPD reconstruction
[power,beamformedData] = coherentFlowPowerDoppler(RcvData, ultrasoundParams,...
    transmit, xCoords, zCoords, 150);

%% Display CFPD image
figure;
imagesc(power(441:856,95:161));
colormap('hot')
xtick = linspace(1,67,7);
xticklabel = linspace(-4.95,4.95,7);

xticks(xtick);
xticklabels(xticklabel);

ytick = 50:50:400;
yticklabel = [15.09;16.63;18.17;19.71;21.25;22.79;24.33;25.87];
yticks(ytick);
yticklabels(yticklabel);
