%% Conventional power doppler reconstruction sequence using IIR filter
%  Power doppler reconstruction using a plane wave coherent compounding
%  beamformer and a 2nd order Butterworth filter.
%  Load data the appropriate RFData file prior to executing this script.

%% Define Imaging Parameters
ultrasoundParams = struct('pitch',0.3/1000,...
                          'fs',50e6,...
                          'f0',6.25e6,...
                          'dopPRF',1000,...
                          'endDepth', 30/1000,...
                          'ensembleLength', 100);

transmit = struct('numAcquisitions', 9,...
                  'steerAngle', linspace(-8*pi/180,8*pi/180,9),...
                  'delays', delays);
c = 1540;
lambda = c/ultrasoundParams.f0;

%% Initialise the coordinate system for image reconstruction.
[xCoords, zCoords] = meshgrid([0:ultrasoundParams.pitch/2:127*ultrasoundParams.pitch],...
    [(1/ultrasoundParams.fs)*c:(1/ultrasoundParams.fs)*c:ultrasoundParams.endDepth]);

% Note: xCoord origin is taking at the centre of the transducer for Field
% II generated data as the steering is pivoted around this centre point.
xCoords = xCoords-127*ultrasoundParams.pitch/2;

%% Perform beamforming and envelope detection
beamformedData = coherentPWCompound2xLatRes(RcvData, ultrasoundParams, transmit, xCoords, zCoords);

IQData = IQDemodulate(beamformedData, ultrasoundParams.f0, ultrasoundParams.fs);

%% Clutter Filtering
% A butterworth created by first estimating the order and cut-off
% frequencies before projection initialisation.
[n,Wn] = buttord(190/500,84/500,3,120);
A = butterProjectionInitialised(n,Wn,ultrasoundParams.ensembleLength);
filteredData = filterEnsemble(A, IQData);

%% Power estimation
power = powerEstimator(filteredData(:,:,25:100),9);

%% Display power doppler image
figure;
imagesc(power(441:856,95:161));
colormap('hot')
xtick = linspace(1,67,7);
xticklabel = linspace(-4.95,4.95,7);

xticks(xtick);
xticklabels(xticklabel);

ytick = 50:50:400;
yticklabel = [15.09;16.63;18.17;19.71;21.25;22.79;24.33;25.87];
yticks(ytick);
yticklabels(yticklabel);
