%% Conventional power doppler reconstruction sequence using IIR filter
%  Power doppler reconstruction using a plane wave coherent compounding
%  beamformer and a 2nd order Butterworth filter.
%  Load data the appropriate RFData file prior to executing this script.

[RFData, delays] = convertVerasonicsRcvData(RcvData, Receive,TX, Trans);

ultrasoundParams = struct('pitch',0.3/1000,...
    'fs',27.7778e6,...
    'f0',6.9444e6,...
    'dopPRF',1000,...
    'endDepth', 45/1000,...
    'ensembleLength', 1);

transmit = struct('numAcquisitions', 7,...
    'steerAngle', linspace(-18*pi/180,18*pi/180,7),...
    'delays', delays);

c = 1540;

% Initialise the coordinate system for image reconstruction.
[xCoords, zCoords] = meshgrid([0:ultrasoundParams.pitch/2:127*ultrasoundParams.pitch],...
    [(1/ultrasoundParams.fs)*c:(1/ultrasoundParams.fs)*c:ultrasoundParams.endDepth]);

% Note: xCoord origin is taking at the centre of the transducer for Field
% II generated data.
% xCoords = xCoords-127*ultrasoundParams.pitch/2;
%beamformedData = coherentPWCompound2xLatRes(RFData(25,1), ultrasoundParams, transmit, xCoords, zCoords);



receiveChannelCoords = repmat(0:ultrasoundParams.pitch:127*ultrasoundParams.pitch,...
                                size(zCoords,1),1);

%%
beamformedData = coherentPWCompound2xLatRes(RFData, ultrasoundParams, transmit, xCoords, zCoords);

IQData = IQDemodulate(beamformedData, ultrasoundParams.f0, ultrasoundParams.fs);

imagesc(abs(IQData(:,:,1)).^0.7);
% [b,a] = butter(2,110/(ultrasoundParams.dopPRF/2), 'high');
% filteredData = filter(b,a,IQData,[],3);
%
% power = powerEstimator(filteredData(:,:,6:20));
% imagesc(power);
% colormap('hot');