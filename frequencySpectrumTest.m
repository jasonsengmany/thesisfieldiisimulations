rearranged = permute(abs(filteredData),[1 3 2]);
%rearranged2 = permute(abs(IQData),[1 3 2]);
insideVessel = [];
%insideVessel2 = [];
outsideVessel = [];
%outsideVessel2 = [];
i = 1;
j = 1;
for row = 441:824
    for column = 95:161
        [f0,power] = analyseFreqSpectrum(rearranged(row,:,column),1000);
        if (insideVesselMask(row,column) == 1) 
            insideVessel = [insideVessel, power];

        elseif(backgroundMask(row,column) == 1) 
            outsideVessel = [outsideVessel, power];
        end
    end
end

avgInsideVessel = mean(insideVessel,2);
plot(f0,avgInsideVessel);
hold on;
avgOutsideVessel = mean(outsideVessel,2);
plot(f0,avgOutsideVessel);

diff = mean(mean(avgInsideVessel(f0 > -287 & f0 < 287)) - mean(avgOutsideVessel(f0 > -287 & f0 < 287)))
    
%plot(f0,mean(insideVessel2,2));
%plot(f0,mean(outsideVessel2,2));
xlabel('frequency (Hz)');
ylabel('power (dB)');
legend('inside vessel (AD)','outside vessel (AD)','inside vessel (before AD)','outside vessel (before AD)');
% i = 0;
% for row = 500:539
%     for column = 112:127
%         i = i + 1;
%         [f0,power] = analyseFreqSpectrum(rearranged(row,:,column),1000);
%         outsideVessel(:) = outsideVessel(:) + power;
%     end
% end
% 
% j = 0;
% for row = 633:645
%     for column = 126:131
%         j = j + 1;
%         [f0,power] = analyseFreqSpectrum(rearranged(row,:,column),1000);
%         insideVessel(:) = insideVessel(:) + power;
%     end
% end


% averageInsideVessel = insideVessel;%./sum(insideVesselMask(441:824,95:161),'all');
% averageOutsideVessel = outsideVessel;%./sum(backgroundMask(441:824,95:161),'all');
% %averageOutsideVessel = outsideVessel./i;
% %averageInsideVessel = insideVessel./j;
% plot(f0,averageInsideVessel);
% hold on;
% plot(f0, averageOutsideVessel);
% xlabel('frequency (Hz)');
% ylabel('power (dB)');
% legend('inside vessel', 'outside vessel');