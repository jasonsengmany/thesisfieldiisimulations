%% Script to initialise field II add-on.
%  Change path as necessary.
addpath('D:\Matlab\R2020a\toolbox\field_II');
%addpath('H:\field_II');
addpath('.\BloodFlowMediaGeneration');
addpath('.\Field_II_RFDataGeneration');
addpath('.\ImageReconstruction');
addpath(genpath('.\Utilities'));
field_init();
